# dns-injector

Allow to inject _dnsPolicy_ and _dnsConfig_ settings through annotations.

## Dependencies

- cert-manager with ca-injector

## Installation

1. Add Helm repo

```bash
helm repo add ydkn https://repo.ydkn.io/charts
```

2. Install the chart on your cluster

```bash
helm install dns-injector ydkn/dns-injector
```

See `deploy/charts/dns-injector/values.yaml` for configuration options.

## Usage

To enable the _dns-injector_ any pod that should be modified needs to have the following label:

```yaml
dns-injector.ydkn.io/enabled: "true"
```

### Change the DNS policy of a pod

Add a pod with the `dns-injector.ydkn.io/policy` annotation with the DNS policy value:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dns-example
  labels:
    dns-injector.ydkn.io/enabled: "true"
  annotations:
    dns-injector.ydkn.io/policy: None
spec:
  containers:
    - name: nginx
      image: nginx
```

This results in the following pod to be scheduled:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dns-example
  labels:
    dns-injector.ydkn.io/enabled: "true"
  annotations:
    dns-injector.ydkn.io/policy: None
spec:
  containers:
    - name: nginx
      image: nginx
  dnsPolicy: None
```

### Change the DNS config of a pod

Create a configmap with a dnsConfig:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-dnsconfig
data:
  dnsConfig.yaml: |
    nameservers:
      - 1.2.3.4
    options:
      - name: ndots
        value: "0"
```

Add a pod with the `dns-injector.ydkn.io/configmap` annotation and the name of the configmap as the value:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dns-example
  labels:
    dns-injector.ydkn.io/enabled: "true"
  annotations:
    dns-injector.ydkn.io/configmap: my-dnsconfig
spec:
  containers:
    - name: nginx
      image: nginx
```

This results in the following pod to be scheduled:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dns-example
  labels:
    dns-injector.ydkn.io/enabled: "true"
  annotations:
    dns-injector.ydkn.io/configmap: my-dnsconfig
spec:
  containers:
    - name: nginx
      image: nginx
  dnsConfig:
    nameservers:
      - 1.2.3.4
    options:
      - name: ndots
        value: "0"
```

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.
