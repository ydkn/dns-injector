package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	dnsinjector "gitlab.com/ydkn/dns-injector/pkg/dnsinjector"
	"go.uber.org/zap"
)

const (
	httpTimeout        = 10 * time.Second
	httpMaxHeaderBytes = 1 << 20 // 1048576
)

type cliArguments struct {
	port     int
	certFile string
	keyFile  string
}

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		fmt.Printf("Failed to initialize logger: %s\n", err.Error()) // nolint:forbidigo
		os.Exit(1)
	}

	var args cliArguments

	flag.IntVar(&args.port, "port", 8443, "Webhook server port.")
	flag.StringVar(&args.certFile, "certFile", "/certs/tls.crt", "File containing the x509 Certificate for HTTPS.")
	flag.StringVar(&args.keyFile, "keyFile", "/certs/tls.key", "File containing the x509 private key to --certFile.")
	flag.Parse()

	injector, err := dnsinjector.NewHandler(dnsinjector.HandlerConfig{Logger: logger.Sugar()})
	if err != nil {
		logger.Fatal(err.Error())
		os.Exit(1)
	}

	logger.Info(fmt.Sprintf("Starting server (:%d) ...", args.port))

	mux := http.NewServeMux()
	mux.HandleFunc("/mutate", injector.Mutate)
	mux.HandleFunc("/healthz", healthCheck)

	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", args.port),
		Handler:        mux,
		ReadTimeout:    httpTimeout,
		WriteTimeout:   httpTimeout,
		MaxHeaderBytes: httpMaxHeaderBytes,
	}

	err = s.ListenAndServeTLS(args.certFile, args.keyFile)
	if err != nil {
		logger.Fatal(err.Error())
	}
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("OK"))
}
