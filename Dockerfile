# Base image
FROM gcr.io/distroless/base

# Add binary
COPY dist/webhook /usr/local/bin/webhook

# Entrypoint
ENTRYPOINT ["webhook"]
