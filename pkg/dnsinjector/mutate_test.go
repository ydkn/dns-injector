package dnsinjector

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	configMap1 = core.ConfigMap{
		ObjectMeta: meta.ObjectMeta{Namespace: "default", Name: "dns-injector-test-0"},
		Data:       map[string]string{"dnsConfig.json": `{ "options": [{ "name": "ndots", "value": "1" }] }`},
	}
	configMap2 = core.ConfigMap{
		ObjectMeta: meta.ObjectMeta{Namespace: "kube-system", Name: "dns-injector-test-1"},
		Data:       map[string]string{"dnsConfig.json": `{ "options": [{ "name": "ndots", "value": "1" }] }`},
	}
)

func TestMutatesRequest(t *testing.T) {
	rawLogger, _ := zap.NewDevelopment()
	logger := rawLogger.Sugar()

	pod := core.Pod{
		ObjectMeta: meta.ObjectMeta{
			Namespace:   configMap1.ObjectMeta.Namespace,
			Name:        "dns-injector-test-pod",
			Annotations: map[string]string{ConfigAnnotation: configMap1.ObjectMeta.Name}},
	}

	client, err := kubeclient()
	assert.NoError(t, err, "failed to create kubernetes client with error %s", err)

	ctx, cancelFunc := context.WithTimeout(context.Background(), kubeTimeout)
	defer cancelFunc()

	configMapsClient := client.CoreV1().ConfigMaps(configMap1.ObjectMeta.Namespace)

	_ = configMapsClient.Delete(ctx, configMap1.ObjectMeta.Name, meta.DeleteOptions{})

	_, err = configMapsClient.Create(ctx, &configMap1, meta.CreateOptions{})
	assert.NoError(t, err, "failed to create config map with error %s", err)

	resp, err := mutate(&pod, logger)
	assert.NoError(t, err, "failed to mutate with error %s", err)

	patches := []map[string]interface{}{}
	err = json.Unmarshal(resp.Patch, &patches)
	assert.NoError(t, err, "failed to unmarshal patches with error %s", err)

	assert.Equal(t, len(patches), 1, "1 patch expected got %d", len(patches))

	pod = core.Pod{
		ObjectMeta: meta.ObjectMeta{
			Namespace:   configMap2.ObjectMeta.Namespace,
			Name:        "dns-injector-test-pod",
			Annotations: map[string]string{ConfigAnnotation: configMap2.ObjectMeta.Name}},
	}

	configMapsClient = client.CoreV1().ConfigMaps(configMap2.ObjectMeta.Namespace)

	_ = configMapsClient.Delete(ctx, configMap2.ObjectMeta.Name, meta.DeleteOptions{})

	_, err = configMapsClient.Create(ctx, &configMap2, meta.CreateOptions{})
	assert.NoError(t, err, "failed to create config map with error %s", err)

	resp, err = mutate(&pod, logger)
	assert.NoError(t, err, "failed to mutate with error %s", err)

	patches = []map[string]interface{}{}
	err = json.Unmarshal(resp.Patch, &patches)
	assert.NoError(t, err, "failed to unmarshal patches with error %s", err)

	assert.Equal(t, len(patches), 1, "1 patch expected got %d", len(patches))
}

func TestMutatesRequestWithoutAnnotation(t *testing.T) {
	rawLogger, _ := zap.NewDevelopment()
	logger := rawLogger.Sugar()

	pod := core.Pod{}

	resp, err := mutate(&pod, logger)
	assert.NoError(t, err, "failed to mutate with error %s", err)

	patches := []map[string]interface{}{}
	err = json.Unmarshal(resp.Patch, &patches)
	assert.NoError(t, err, "failed to unmarshal patches with error %s", err)

	assert.Equal(t, len(patches), 0, "0 patches expected got %d", len(patches))
}
