package dnsinjector

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	admission "k8s.io/api/admission/v1"
	core "k8s.io/api/core/v1"
)

// HandlerConfig configuration for a DNS injector handler.
type HandlerConfig struct {
	Logger *zap.SugaredLogger
}

// Handler handle DNS injector webhook requests.
type Handler struct {
	logger *zap.SugaredLogger
}

// NewHandler creates a new Handler with given configuration.
func NewHandler(config HandlerConfig) (*Handler, error) {
	return &Handler{logger: config.Logger}, nil
}

// Mutate handler for mutate webhook.
func (h *Handler) Mutate(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		h.writeError(w, err)

		return
	}

	var admissionReview admission.AdmissionReview
	if err = json.Unmarshal(body, &admissionReview); err != nil {
		h.writeError(w, err)

		return
	}

	if admissionReview.Request == nil {
		return
	}

	var pod core.Pod
	if err := json.Unmarshal(admissionReview.Request.Object.Raw, &pod); err != nil {
		h.writeError(w, err)

		return
	}

	response, err := mutate(&pod, h.logger)
	if err != nil {
		h.writeError(w, err)

		return
	}

	h.logger.Infow(
		"applying patches",
		"pod", pod.ObjectMeta.Name,
		"namespace", pod.ObjectMeta.Namespace,
		"patches", string(response.Patch),
	)

	admissionReview.Response = &response
	admissionReview.Response.UID = admissionReview.Request.UID

	responseBody, err := json.Marshal(admissionReview)
	if err != nil {
		h.writeError(w, err)

		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(responseBody)
}

func (h *Handler) writeError(w http.ResponseWriter, err error) {
	h.logger.Errorw(err.Error())

	w.WriteHeader(http.StatusInternalServerError)
	_, _ = w.Write([]byte(err.Error()))
}
