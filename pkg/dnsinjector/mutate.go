package dnsinjector

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"go.uber.org/zap"
	yaml "gopkg.in/yaml.v3"
	admission "k8s.io/api/admission/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const kubeTimeout = 10 * time.Second

func mutate(pod *core.Pod, logger *zap.SugaredLogger) (admission.AdmissionResponse, error) {
	var err error

	patchType := admission.PatchTypeJSONPatch

	resp := admission.AdmissionResponse{
		Allowed:          true,
		PatchType:        &patchType,
		AuditAnnotations: map[string]string{},
		Result:           &meta.Status{Status: "Success"},
	}

	patches := []map[string]interface{}{}

	if policy, ok := pod.ObjectMeta.Annotations[PolicyAnnotation]; ok {
		logger.Debugw("policy annotation found", "policy", policy)

		resp.AuditAnnotations["policy"] = policy

		patch := mutateDNSPolicy(policy)
		if _, ok := patch["op"]; ok {
			patches = append(patches, patch)
		}
	}

	if configName, ok := pod.ObjectMeta.Annotations[ConfigAnnotation]; ok {
		logger.Debugw("config annotation found", "configName", configName)

		resp.AuditAnnotations["config"] = configName

		patch, err := mutateDNSConfig(pod, configName, logger)
		if err != nil {
			return resp, err
		}

		if _, ok := patch["op"]; ok {
			patches = append(patches, patch)
		}
	} else {
		logger.Debugw("no config annotation")
	}

	resp.Patch, err = json.Marshal(patches)
	if err != nil {
		return resp, fmt.Errorf("failed to convert patch to JSON: %w", err)
	}

	return resp, nil
}

func mutateDNSPolicy(policy string) map[string]interface{} {
	if policy == "" {
		return map[string]interface{}{}
	}

	return map[string]interface{}{
		"op":    "replace",
		"path":  "/spec/dnsPolicy",
		"value": policy,
	}
}

func mutateDNSConfig(pod *core.Pod, configMapName string, logger *zap.SugaredLogger) (map[string]interface{}, error) {
	patch := map[string]interface{}{}

	configMap, err := getConfigMap(pod.Namespace, configMapName)
	if errors.IsNotFound(err) {
		logger.Errorw("configmap does not exist", "configMapName", configMapName)

		return patch, nil
	} else if err != nil {
		return patch, err
	}

	if configMap == nil {
		logger.Errorw("configmap does not exist", "configMapName", configMapName)

		return patch, nil
	}

	dnsConfig := core.PodDNSConfig{}

	if data, ok := configMap.Data[ConfigMapDNSConfigKeyJSON]; ok {
		err = json.Unmarshal([]byte(data), &dnsConfig)
		if err != nil {
			return patch, fmt.Errorf("failed to parse dnsConfig JSON from configmap: %w", err)
		}
	} else if data, ok := configMap.Data[ConfigMapDNSConfigKeyYAML]; ok {
		err = yaml.Unmarshal([]byte(data), &dnsConfig)
		if err != nil {
			return patch, fmt.Errorf("failed to parse dnsConfig YAML from configmap: %w", err)
		}
	} else {
		logger.Errorw("config data not found", "configMapName", configMapName)

		return patch, nil
	}

	dnsConfigOp := "add"
	if pod.Spec.DNSConfig != nil {
		dnsConfigOp = "replace"
	}

	return map[string]interface{}{
		"op":    dnsConfigOp,
		"path":  "/spec/dnsConfig",
		"value": dnsConfig,
	}, nil
}

func getConfigMap(podNamespace, configMapName string) (*core.ConfigMap, error) {
	ns := podNamespace
	name := configMapName

	nameParts := strings.SplitN(configMapName, "/", 2)
	if len(nameParts) > 1 {
		ns = nameParts[0]
		name = nameParts[1]
	}

	client, err := kubeclient()
	if err != nil {
		return nil, err
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), kubeTimeout)
	defer cancelFunc()

	configMap, err := client.CoreV1().ConfigMaps(ns).Get(ctx, name, meta.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to get configmap: %w", err)
	}

	return configMap, nil
}
