package dnsinjector

import (
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func kubeconfig() (*rest.Config, error) {
	kubeconfigEnv := os.Getenv("KUBECONFIG")

	if kubeconfigEnv != "" {
		return clientcmd.BuildConfigFromFlags("", kubeconfigEnv)
	}

	return rest.InClusterConfig()
}

func kubeclient() (*kubernetes.Clientset, error) {
	config, err := kubeconfig()
	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfig(config)
}
