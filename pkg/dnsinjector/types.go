package dnsinjector

import "fmt"

// ErrorConfigNotExists referenced config could not be found.
var ErrorConfigNotExists = fmt.Errorf("config does not exist: ")

const (
	// PolicyAnnotation annotation name for dns policy.
	PolicyAnnotation = "dns-injector.ydkn.io/policy"

	// ConfigAnnotation annotation name for dns config.
	ConfigAnnotation = "dns-injector.ydkn.io/configmap"

	// ConfigMapDNSConfigKeyJSON data key for dnsConfig in JSON format.
	ConfigMapDNSConfigKeyJSON = "dnsConfig.json"

	// ConfigMapDNSConfigKeyYAML data key for dnsConfig in YAML format.
	ConfigMapDNSConfigKeyYAML = "dnsConfig.yaml"
)
